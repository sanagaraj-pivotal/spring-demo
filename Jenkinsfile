pipeline {
    agent {
        docker {
            image 'sandeepnvmware/jenkins-node:latest'
        }
    }

    environment {
        BUILDER = 'my-builder'
        SERVICE_ACCOUNT = 'spring-demo-service-account'
        APP_NAME='spring-demo-app'
        GROUP_TO_SEND_EMAIL_NOTIFICATIONS='somegroup@somehost.com'
    }

    post {
        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }

        failure {
            script {
                if (env.BRANCH_NAME == 'main') {
                    emailext (body: '''${SCRIPT, template="groovy-html.template"}''',
                                mimeType: 'text/html',
                                subject: 'Build failing for  ${APP_NAME} ${JOB_NAME} project is failing please fix',
                                to: 'hi@hello.com')
                }
            }
        }
    }

    options {
        ansiColor('xterm')
        disableConcurrentBuilds()
        gitLabConnection('gitlab-auth')
    }

    stages {
        stage('Notify gitlab') {
            steps {
                updateGitlabCommitStatus name: 'build', state: 'running'
            }
        }
        stage('Building branch') {
            steps {
                sh 'git config --global user.email "admin@example.com"'
                sh 'git config --global user.name "Admin"'
                sh 'git merge origin/main'
            }
        }
//         stage('Build') {
//             steps {
//                 sh 'mvn -B -DskipTests clean package'
//             }
//         }
//
        stage('SonarQube Analysis') {
           steps {
               withSonarQubeEnv(credentialsId: 'sonarqube-global-token', installationName: 'My SonarQube Server') {
                    sh 'echo $BRANCH_NAME'
                    sh 'mvn clean verify sonar:sonar -Pcoverage -Dsonar.projectKey=$APP_NAME'
               }
           }
        }
//
//         stage("Quality gate") {
//             steps {
//                 waitForQualityGate abortPipeline: true
//             }
//         }

        stage('Build image') {
            when {
                branch 'main'
            }
            steps {
                withKubeConfig([credentialsId: 'kube-config-file']) {
                    sh 'kp image save $APP_NAME-$GIT_COMMIT --tag $DOCKER_USER_NAME/$APP_NAME:$GIT_COMMIT --git $GIT_URL --git-revision $GIT_COMMIT --builder $BUILDER --service-account $SERVICE_ACCOUNT --env "BP_JVM_VERSION=17"'
                    sh 'kp build logs $APP_NAME-$GIT_COMMIT'
                    sh 'kp builds list $APP_NAME-$GIT_COMMIT | grep -q SUCCESS'
                    script {
                        tmp_param =  sh (script: 'kubectl get image $APP_NAME-$GIT_COMMIT -o json | jq -r .status.latestImage', returnStdout: true).trim()
                        env.image = tmp_param
                    }
                }
            }
        }

        stage('Generate descriptor') {
            when {
                branch 'main'
            }
            steps {

                withCredentials([gitUsernamePassword(credentialsId: 'git-credentials',
                                 gitToolName: 'git-tool')]) {

                    dir("gitops") {
                      deleteDir()
                    }


                    sh 'git clone https://gitlab.com/sanagaraj-pivotal/gitops.git'
                    sh 'git config --global user.email "admin@example.com"'
                    sh 'git config --global user.name "Admin"'

                    dir("gitops") {
                           sh 'yq -i $(echo .apps."$APP_NAME"_image=\\""$image\\"" | sed \'s/spring-demo-app/spring_demo_app/\') commons.yml'
                           sh 'cat commons.yml'
                           sh 'git add commons.yml && git commit --allow-empty -m "Updating $APP_NAME version"'
                           sh 'push-only-if-changes-found.sh'
                    }
               }
            }
        }
    }
}
