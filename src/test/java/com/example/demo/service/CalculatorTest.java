package com.example.demo.service;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CalculatorTest {

    @Autowired
    private Calculator target;

    @Test
    public void itDivides() {

        assertEquals(10, target.divide(100, 10));
    }
}
