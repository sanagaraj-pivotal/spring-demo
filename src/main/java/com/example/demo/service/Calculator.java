package com.example.demo.service;

import org.springframework.stereotype.Service;

@Service
public class Calculator {
    public int divide(int numerator, int divisor) {

        if (divisor == 0) {
            return Integer.MAX_VALUE;
        }

        return numerator / divisor;
    }
}
