# Sonarqube / Jenkins configuration

## Create a project
- Create Project (Manually)
```
Project display name
Project key
```

## Sonarqube: Generate a Token
- A token needs to be generated to connect to Sonarqube from Jenkins.
- Go to My Account / Security/ Generate Tokens  from the http://localhost:9000/account/security page 
- For each project, generate a Global Analysis Token (with No Expiration date)

## Jenkins: Add a new credential to save the token

### Sonarqube Plugin Configuration
- Install the SonarQube plugin
- Manage Jenkins / Configure System / Add Sonarqube
- Name: <Sonarqube Installation Name>
- Server URL: <the Sonarqube URL>
- Server authentication token: <None>

### Credential

- For each job (http://localhost:8080/job/spring-demo/), go to Jenkins Project configuration to create a new credential with the value of the generated token.

- Go to credentials in the job section: "Stores scoped to <job name>" / "Global credentials (unrestricted)" / "Add Credentials" / "Secret Text"

```
Secret: <Generated Token>
ID: <sonarqube-global-token>
```

## Update the Jenkinsfile to add a SonarQube Analysis step

In the Jenkinsfile, update the values with the values used in the previous section:

- credentialsId: 'sonarqube-global-token'
- installationName: 'My SonarQube Server'
- sonar.projectKey=$APP_NAME   (the APP_NAME environment variable is defined at the begining of the Jenkinsfile)

```
        stage('SonarQube Analysis') {
           steps {
               withSonarQubeEnv(credentialsId: 'sonarqube-global-token', installationName: 'My SonarQube Server') {
                 sh "mvn clean verify sonar:sonar -Dsonar.projectKey=$APP_NAME"
               }
           }
        }
```

## Add Sonarqube Maven Plugin to the project
```xml
<plugin>
    <groupId>org.sonarsource.scanner.maven</groupId>
    <artifactId>sonar-maven-plugin</artifactId>
    <version>3.7.0.1746</version>
</plugin>
```


